﻿using Coscine.Configuration;
using Coscine.Database.Models;
using Polly;
using System.Globalization;
using VDS.RDF;
using VDS.RDF.Parsing;
using VDS.RDF.Query;
using VDS.RDF.Storage;
using VDS.RDF.Update;

namespace TrellisMigrator
{
    public abstract class StructuralData<S, T> where S : class where T : DatabaseModel<S>, new()
    {
        public T Model { get; init; }
        public ConsulConfiguration Configuration { get; init; }
        public static string Prefix { get; set; }

        public readonly SparqlRemoteUpdateEndpoint UpdateEndpoint;
        public readonly SparqlRemoteEndpoint QueryEndpoint;
        public readonly ReadWriteSparqlConnector ReadWriteSparqlConnector;

        public StructuralData()
        {
            Configuration = new ConsulConfiguration();
            Model = new T();
            Prefix = Configuration.GetStringAndWait("coscine/global/epic/prefix");

            var sparqlEndpoint = Configuration.GetStringAndWait("coscine/local/virtuoso/additional/url");

            UpdateEndpoint = new SparqlRemoteUpdateEndpoint(new Uri(string.Format(sparqlEndpoint)));
            QueryEndpoint = new SparqlRemoteEndpoint(new Uri(string.Format(sparqlEndpoint)));
            ReadWriteSparqlConnector = new ReadWriteSparqlConnector(QueryEndpoint, UpdateEndpoint);
            // 100 second timeout
            var timeout = 100000;
            QueryEndpoint.Timeout = timeout;
            UpdateEndpoint.Timeout = timeout;
            ReadWriteSparqlConnector.Timeout = timeout;
        }

        public abstract IEnumerable<IGraph> ConvertToLinkedData(IEnumerable<S> entries);

        public virtual IEnumerable<S> GetAll()
        {
            return Model.GetAll();
        }

        public void Migrate(bool dummyMode)
        {
            var spacer = new string('-', 35);
            Console.WriteLine($"\n{spacer}\n{typeof(T).Name}\n{spacer}");
            var graphs = ConvertToLinkedData(GetAll());
            if (!dummyMode)
            {
                StoreGraphs(graphs);
            }
        }

        private void StoreGraphs(IEnumerable<IGraph> graphs)
        {
            foreach (var graph in graphs)
            {
                Console.WriteLine($" ({graph.BaseUri})");

                if (graph is WrapperGraph)
                {
                    var wrapperGraph = (WrapperGraph)graph;
                    // Chunking since the size otherwise can be too large
                    foreach (var triples in wrapperGraph.AssertList.Chunk(100))
                    {
                        WrapRequest(() => ReadWriteSparqlConnector.UpdateGraph(graph.BaseUri, triples, new List<Triple>()));
                    }
                    // Chunking since the size otherwise can be too large
                    foreach (var triples in wrapperGraph.RetractList.Chunk(100))
                    {
                        WrapRequest(() => ReadWriteSparqlConnector.UpdateGraph(graph.BaseUri, new List<Triple>(), triples));
                    }
                }
                else
                {
                    var exists = WrapRequest(() => HasGraph(graph.BaseUri));
                    if (exists)
                    {
                        Console.WriteLine($" - Graph {graph.BaseUri} exists");

                        // Clear the existing graph from the store
                        WrapRequest(() => ClearGraph(graph.BaseUri));
                        Console.WriteLine($" - Cleared Graph {graph.BaseUri}");
                    }

                    // Chunking since the size otherwise can be too large
                    // Don't change to only addition of triples, otherwise this could break things
                    foreach (var triples in graph.Triples.Chunk(100))
                    {
                        WrapRequest(() => ReadWriteSparqlConnector.UpdateGraph(graph.BaseUri, triples, new List<Triple>()));
                    }
                }

                Console.WriteLine($" - Graph {graph.BaseUri} added successfully");
                Console.WriteLine();
            }
        }

        public void AssertToGraphUriNode(IGraph graph, string graphSubject, string graphPredicate, string? graphObject)
        {
            if (graphObject != null)
            {
                graph.Assert(
                    new Triple(
                        graph.CreateUriNode(new Uri(graphSubject)),
                        graph.CreateUriNode(new Uri(graphPredicate)),
                        graph.CreateUriNode(new Uri(graphObject))
                    )
                );
            }
        }

        public void AssertToGraphLiteralNode(IGraph graph, string graphSubject, string graphPredicate, string? graphObject, Uri? objectType = null)
        {
            if (graphObject != null)
            {
                graph.Assert(
                    new Triple(
                        graph.CreateUriNode(new Uri(graphSubject)),
                        graph.CreateUriNode(new Uri(graphPredicate)),
                        graph.CreateLiteralNode(graphObject, objectType)
                    )
                );
            }
        }

        public void AssertToGraphBlankAndUriNode(IGraph graph, IBlankNode graphSubject, string graphPredicate, string? graphObject)
        {
            if (graphObject != null)
            {
                graph.Assert(
                    new Triple(
                        graphSubject,
                        graph.CreateUriNode(new Uri(graphPredicate)),
                        graph.CreateUriNode(new Uri(graphObject))
                    )
                );
            }
        }

        public static void AddModifiedDate(IGraph graph, IUriNode dctermsModifiedNode, IUriNode rootNode)
        {
            if (!graph.GetTriplesWithSubjectPredicate(rootNode, dctermsModifiedNode).Any())
            {
                graph.Assert(new Triple(
                    rootNode,
                    dctermsModifiedNode,
                    graph.CreateLiteralNode(
                        DateTime.UtcNow.ToString("o", CultureInfo.InvariantCulture),
                        new Uri(XmlSpecsHelper.XmlSchemaDataTypeDateTime)
                    )
                ));
            }
        }

        public bool HasGraph(string graphName)
        {
            return HasGraph(new Uri(graphName));
        }

        public bool HasGraph(Uri graphUri)
        {
            return !GetGraph(graphUri).IsEmpty;
        }

        // Returns an empty graph, if the graph does not exists
        public IGraph GetGraph(string graphName)
        {
            return GetGraph(new Uri(graphName));
        }

        // Returns an empty graph, if the graph does not exists
        public IGraph GetGraph(Uri graphUri)
        {
            var graph = new WrapperGraph();

            ReadWriteSparqlConnector.LoadGraph(graph, graphUri.AbsoluteUri);

            graph.AssertList.Clear();
            graph.RetractList.Clear();

            return graph;
        }


        public void ClearGraph(string graphName)
        {
            ClearGraph(new Uri(graphName));
        }

        public void ClearGraph(Uri graphUri)
        {
            SparqlParameterizedString queryString = new SparqlParameterizedString
            {
                CommandText = "CLEAR GRAPH @graph"
            };
            queryString.SetUri("graph", graphUri);
            QueryEndpoint.QueryRaw(queryString.ToString());
        }

        /// <summary>
        /// Retry Virtuoso Requests since they sometimes just fail
        /// </summary>
        /// <typeparam name="W"></typeparam>
        /// <param name="function"></param>
        /// <returns></returns>
        public void WrapRequest(Action action)
        {
            Policy
                .Handle<Exception>()
                .WaitAndRetry(5, retryNumber => TimeSpan.FromMilliseconds(200))
                .Execute(() => action.Invoke());
        }

        /// <summary>
        /// Retry Virtuoso Requests since they sometimes just fail
        /// </summary>
        /// <typeparam name="W"></typeparam>
        /// <param name="function"></param>
        /// <returns></returns>
        public W WrapRequest<W>(Func<W> function)
        {
            return Policy
                .Handle<Exception>()
                .WaitAndRetry(5, retryNumber => TimeSpan.FromMilliseconds(200))
                .ExecuteAndCapture(() => function.Invoke()).Result;
        }

    }
}
