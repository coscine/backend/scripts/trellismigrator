using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Database.Util;
using VDS.RDF;
using VDS.RDF.Query;

namespace TrellisMigrator.Implementations
{
    public class ResourceStructuralData : StructuralData<Resource, ResourceModel>
    {

        // Override to also receive deleted resources
        public override IEnumerable<Resource> GetAll()
        {
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return
                    (from tableEntry in Model.GetITableFromDatabase(db)
                     select tableEntry).ToList();
            });
        }

        public IEnumerable<Uri> ListGraphs(string id)
        {
            var cmdString = new SparqlParameterizedString
            {
                CommandText = @"SELECT DISTINCT ?g
                WHERE { GRAPH ?g { ?s ?p ?o }
                FILTER(contains(str(?g), @graph)) }"
            };
            cmdString.SetLiteral("graph", id);

            var resultSet = QueryEndpoint.QueryWithResultSet(cmdString.ToString());

            var graphs = new List<Uri>();
            foreach (SparqlResult r in resultSet)
            {
                graphs.Add((r.Value("g") as UriNode)?.Uri);
            }
            return graphs;
        }

        public IEnumerable<Uri> ListFileGraphs(string id)
        {
            var cmdString = new SparqlParameterizedString
            {
                CommandText = @"SELECT DISTINCT ?g
                WHERE { GRAPH ?g { ?s a <http://purl.org/fdp/fdp-o#MetadataService> }
                FILTER(contains(str(?g), @graph)) }"
            };
            cmdString.SetLiteral("graph", id);

            var resultSet = QueryEndpoint.QueryWithResultSet(cmdString.ToString());

            var graphs = new List<Uri>();
            foreach (SparqlResult r in resultSet)
            {
                graphs.Add((r.Value("g") as UriNode)?.Uri);
            }
            return graphs;
        }

        public override IEnumerable<IGraph> ConvertToLinkedData(IEnumerable<Resource> entries)
        {
            var graph = WrapRequest(() => GetGraph("http://www.trellisldp.org/ns/trellis#PreferServerManaged"));
            var graphs = new List<IGraph> { graph };

            var partOfNode = graph.CreateUriNode(new Uri("http://purl.org/dc/terms/isPartOf"));
            var basicContainerNode = graph.CreateUriNode(new Uri("http://www.w3.org/ns/ldp#BasicContainer"));

            var aNode = graph.CreateUriNode(new Uri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"));
            var dctermsModifiedNode = graph.CreateUriNode(new Uri("http://purl.org/dc/terms/modified"));

            var resourceUrlPrefix = "https://purl.org/coscine/resources";
            var resourcesNode = graph.CreateUriNode(new Uri(resourceUrlPrefix));

            var baseUrlPrefix = "https://purl.org/coscine/";
            var baseNode = graph.CreateUriNode(new Uri(baseUrlPrefix));

            foreach (var entry in entries)
            {
                var resourceGraphName = $"{resourceUrlPrefix}/{entry.Id}";
                Console.WriteLine($"Processing {resourceGraphName}");

                var resourceUri = new Uri(resourceGraphName);
                var resourceNode = graph.CreateUriNode(resourceUri);

                var triples = graph.GetTriplesWithSubject(resourceUri);
                if (!triples.Any())
                {
                    graph.Assert(new Triple(
                        resourceNode,
                        aNode,
                        basicContainerNode
                    ));
                    AddModifiedDate(graph, dctermsModifiedNode, resourceNode);
                    graph.Assert(new Triple(
                        resourceNode,
                        partOfNode,
                        resourcesNode
                    ));
                    graph.Assert(new Triple(
                        resourcesNode,
                        aNode,
                        basicContainerNode
                    ));
                    AddModifiedDate(graph, dctermsModifiedNode, resourcesNode);
                    graph.Assert(new Triple(
                        resourcesNode,
                        partOfNode,
                        baseNode
                    ));
                    graph.Assert(new Triple(
                        baseNode,
                        aNode,
                        basicContainerNode
                    ));
                    AddModifiedDate(graph, dctermsModifiedNode, baseNode);
                }

                var resourceACLGraphName = $"{resourceUrlPrefix}/{entry.Id}?ext=acl";

                var entryAlreadyExists = WrapRequest(() => HasGraph(resourceACLGraphName));
                if (!entryAlreadyExists)
                {
                    var resourceGraphExists = WrapRequest(() => HasGraph(resourceGraphName));
                    if (resourceGraphExists)
                    {
                        var resourceGraph = WrapRequest(() => GetGraph(resourceGraphName));
                        var aclSubjects = resourceGraph.GetTriplesWithObject(new Uri("http://www.w3.org/ns/auth/acl#Authorization")).Select((triple) => triple.Subject);

                        var aclGraph = new Graph()
                        {
                            BaseUri = new Uri(resourceACLGraphName)
                        };

                        foreach (var subject in aclSubjects)
                        {
                            var subjectNode = aclGraph.CreateBlankNode();
                            foreach (var triple in resourceGraph.GetTriplesWithSubject(subject))
                            {
                                aclGraph.Assert(new Triple(
                                    subjectNode,
                                    Tools.CopyNode(triple.Predicate, aclGraph),
                                    Tools.CopyNode(triple.Object, aclGraph)
                                ));
                            }
                        }

                        graphs.Add(aclGraph);
                    }
                }
            }

            return graphs;
        }

    }
}
