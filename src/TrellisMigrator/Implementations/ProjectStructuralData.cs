﻿using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Database.Util;
using VDS.RDF;

namespace TrellisMigrator.Implementations
{
    public class ProjectStructuralData : StructuralData<Project, ProjectModel>
    {
        // Override to also receive deleted projects
        public override IEnumerable<Project> GetAll()
        {
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return
                    (from tableEntry in Model.GetITableFromDatabase(db)
                     select tableEntry).ToList();
            });
        }

        public override IEnumerable<IGraph> ConvertToLinkedData(IEnumerable<Project> entries)
        {
            var graph = WrapRequest(() => GetGraph("http://www.trellisldp.org/ns/trellis#PreferServerManaged"));
            var graphs = new List<IGraph> { graph };

            var partOfNode = graph.CreateUriNode(new Uri("http://purl.org/dc/terms/isPartOf"));
            var basicContainerNode = graph.CreateUriNode(new Uri("http://www.w3.org/ns/ldp#BasicContainer"));

            var aNode = graph.CreateUriNode(new Uri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"));
            var dctermsModifiedNode = graph.CreateUriNode(new Uri("http://purl.org/dc/terms/modified"));

            var projectUrlPrefix = "https://purl.org/coscine/projects";
            var projectsNode = graph.CreateUriNode(new Uri(projectUrlPrefix));

            var baseUrlPrefix = "https://purl.org/coscine/";
            var baseNode = graph.CreateUriNode(new Uri(baseUrlPrefix));

            foreach (var entry in entries)
            {
                var projectGraphName = $"{projectUrlPrefix}/{entry.Id}";
                var projectUri = new Uri(projectGraphName);
                var projectNode = graph.CreateUriNode(projectUri);

                var triples = graph.GetTriplesWithSubject(projectUri);
                if (!triples.Any())
                {
                    graph.Assert(new Triple(
                        projectNode,
                        aNode,
                        basicContainerNode
                    ));
                    AddModifiedDate(graph, dctermsModifiedNode, projectNode);
                    graph.Assert(new Triple(
                        projectNode,
                        partOfNode,
                        projectsNode
                    ));
                    graph.Assert(new Triple(
                        projectsNode,
                        aNode,
                        basicContainerNode
                    ));
                    AddModifiedDate(graph, dctermsModifiedNode, projectsNode);
                    graph.Assert(new Triple(
                        projectsNode,
                        partOfNode,
                        baseNode
                    ));
                    graph.Assert(new Triple(
                        baseNode,
                        aNode,
                        basicContainerNode
                    ));
                    AddModifiedDate(graph, dctermsModifiedNode, baseNode);
                }
            }
            return graphs;
        }
    }
}