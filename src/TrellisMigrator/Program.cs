﻿using TrellisMigrator.Implementations;

var dummyMode = !(args.Length > 0 && args[0] == "--noDryRun");
if (dummyMode)
{
    Console.WriteLine("\n DUMMY MODE \n");
    Console.WriteLine(" To exit dummy mode, execute with the \"--noDryRun\" argument");
}

Console.WriteLine("\nBegin Trellis migration");

var projectStructuralData = new ProjectStructuralData();
projectStructuralData.Migrate(dummyMode);

var resourceStructuralData = new ResourceStructuralData();
resourceStructuralData.Migrate(dummyMode);

Console.WriteLine("\n Finished.");
