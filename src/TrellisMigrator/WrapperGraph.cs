﻿using VDS.RDF;

namespace TrellisMigrator
{
    public class WrapperGraph : Graph
    {
        public List<Triple> AssertList { get; set; }
        public List<Triple> RetractList { get; set; }

        public WrapperGraph() : base()
        {
            AssertList = new List<Triple>();
            RetractList = new List<Triple>();
        }

        public override bool Assert(Triple t)
        {
            AssertList.Add(t);
            return base.Assert(t);
        }

        public override bool Assert(IEnumerable<Triple> triples)
        {
            AssertList.AddRange(triples);
            return base.Assert(triples);
        }

        public override bool Retract(Triple t)
        {
            RetractList.Add(t);
            return base.Retract(t);
        }

        public override bool Retract(IEnumerable<Triple> triples)
        {
            RetractList.AddRange(triples);
            return base.Retract(triples);
        }
    }
}
